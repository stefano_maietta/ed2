#include "Jugador.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>

Jugador::Jugador(string val, string nombre) : _val(val), _nombre(nombre), _piezas(10)
{

}

Pieza* Jugador::encontrarFicha(int row, int col)
{
	int cellRow;
	int cellCol;
	for (Pieza* pieza : _piezas){
		cellRow = pieza->getCelda()->getFila();
		cellCol = pieza->getCelda()->getColumna();
		if (cellRow == row && cellCol == col)
		{
			return pieza;
		}
	}
		
	return nullptr;
}

Pieza* Jugador::getFichaAleatoria()
{
	srand(time(NULL));
	int random = rand() % _piezas.size();
	return _piezas[random];
}

