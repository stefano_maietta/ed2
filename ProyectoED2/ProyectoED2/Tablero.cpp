#include "Tablero.h"


Tablero::~Tablero()
{

}

Tablero::Tablero(int numJugadores) : _numPlayers(numJugadores)
{
	createMap();
}

//Direccion 1 derecha, -1 izquierda
vector<Celda*> Tablero::celdaDisponible(int row, int col, int direction)
{
	vector<Celda*> celdas;
	//Derecha
	if (map[row][col + 1]->isEmpty() && map[row][col + 1]->isValid())
	{
		//validacion campo extra
		celdas.push_back(map[row][col + 1]);
	}
	//Izquierda
	if (map[row][col - 1]->isEmpty() && map[row][col - 1]->isValid())
	{
		celdas.push_back(map[row][col - 1]);
	}
	//Arriba
	if (map[row + 1][col]->isEmpty() && map[row + 1][col]->isValid())
	{
		celdas.push_back(map[row + 1][col]);
	}
	//Abajo
	if (map[row - 1][col]->isEmpty() && map[row - 1][col]->isValid())
	{
		celdas.push_back(map[row - 1][col]);
	}
	//Arriba a la Derecha o Arriba a la Izquierda
	if (map[row + 1][col + direction]->isEmpty() && map[row + 1][col + direction]->isValid())
	{
		celdas.push_back(map[row + 1][col + direction]);
	}
	//Abajo a la derecha o Abajo a la izquierda
	if (map[row - 1][col + direction]->isEmpty() && map[row - 1][col + direction]->isValid())
	{
		celdas.push_back(map[row - 1][col + direction]);
	}

	return celdas;
}


void Tablero::createMap()
{
	Celda* celdaInvalida = new Celda(-1, -1, false);
	for (int i = 0; i < 17; i++)
	{
		for (int j = 0; j < 13; j++)
		{
			if (i == 0 || i == 16)
			{
				if (j != 6)
				{
					map[i][j] = celdaInvalida;
				}
			}
			else if (i == 1 || i == 15)
			{
				if (j < 5 || j > 6)
				{
					map[i][j] = celdaInvalida;
				}
			}
			else if (i == 2 || i == 14)
			{
				if (j < 5 || j > 7)
				{
					map[i][j] = celdaInvalida;
				}
			}
			else if (i == 12)
			{
				if (j < 4 || j > 8)
				{
					map[i][j] = celdaInvalida;
				}
			}
			else if (i == 3 || i == 13)
			{
				if (j < 4 || j > 7)
				{
					map[i][j] = celdaInvalida;
				}
			}

			else if (i == 5 || i == 10)
			{
				if (j == 12 || j == 0)
				{
					map[i][j] = celdaInvalida;
				}
			}

			else if (i == 6 || i == 9)
			{
				if (j < 2 || j > 10)
				{
					map[i][j] = celdaInvalida;
				}
			}

			else if (i == 7 || i == 8)
			{
				if (j < 3 || j > 9)
				{
					map[i][j] = celdaInvalida;
				}
			}

			if (map[i][j] != celdaInvalida)
			{
				map[i][j] = new Celda(i, j, true);
			}
		}
	}
}

vector<Celda*> Tablero::getMovimientosPosibles(Celda* from)
{
	vector<Celda*> celdasDisponibles;
	int row = from->getFila();
	int col = from->getColumna();
	if (row % 2 == 0)
	{
		celdasDisponibles = celdaDisponible(row, col, -1);
	}
	else
	{
		celdasDisponibles = celdaDisponible(row, col, 1);
	}
	return celdasDisponibles;
}
