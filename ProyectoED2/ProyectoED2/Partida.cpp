#include "Partida.h"

Partida::~Partida()
{
	//dtor
}

Partida::Partida(vector<Jugador*> jugadores, Tablero* tablero) : _jugadores(jugadores),
_tablero(tablero)
{
	init();

}

vector<Celda*> Partida::getMovimientosPosibles(int row, int col)
{
	vector<Celda*> moves;
	Pieza* pieza;
	pieza = _jugadorActual->encontrarFicha(row, col);
	if (pieza != nullptr)
	{
		moves = _tablero->getMovimientosPosibles(pieza->getCelda());
	}
	else
	{
		cout << "No se encontraron movimientos" << endl;
	}

	return moves;
}

void Partida::init()
{
	_jugadorActual = _jugadores[0];

}

void Partida::moverPieza(Pieza* pieza, Celda* destino)
{
	pieza->getCelda()->clean();
	pieza->setCelda(destino);
	destino->setPieza(pieza);
	if (_count == _jugadores.size() - 1)
	{
		_count = 0;
	}
	else
	{
		_count++;
	}
	_jugadorActual = _jugadores[_count];
}

void Partida::asignarPiezas()
{
	//1� Jugador
	Pieza* pieza;
	for (int i = 0; i<4; i++){
		for (int j = 4; j<8; j++){
			if (_tablero->map[i][j]->isValid()){
				pieza = new Pieza(_jugadores[0]->getVal());
				pieza->setCelda(_tablero->map[i][j]);
				pieza->getCelda()->setPieza(pieza);
				_jugadores[0]->addPiece(pieza);
				
			}
		}
	}
	//2� Jugador
	for (int i = 13; i<17; i++)
	{
		for (int j = 4; j<8; j++)
		{
			if (_tablero->map[i][j]->isValid())
			{
				pieza = new Pieza(_jugadores[1]->getVal());
				pieza->setCelda(_tablero->map[i][j]);
				pieza->getCelda()->setPieza(pieza);
				_jugadores[1]->addPiece(pieza);
			}
		}
	}
}




