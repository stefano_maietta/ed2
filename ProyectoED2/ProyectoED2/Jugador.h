#ifndef JUGADOR_H
#define JUGADOR_H

#include <string>
#include <vector>
#include "Pieza.h"

using std::string;
using std::vector;

class Jugador
{
public:
	Jugador(string val, string nombre);
	bool moverPieza();
	inline string getNombre() { return _nombre; }
	string getVal() { return _val; }
	Pieza* encontrarFicha(int row, int col);
	Pieza* getFichaAleatoria();
	inline vector<Pieza*> getPiezas(){ return _piezas; }
	inline void addPiece(Pieza* pieza)
	{
		_piezas[_counter] = pieza;
		_counter++;
	}
private:
	string _nombre;
	string _val;
	vector<Pieza*> _piezas;
	int _counter = 0;

};

#endif