#include "Celda.h"

Celda::Celda(int row, int column, bool valid) : _fila(row),
_columna(column), _valid(valid), _pieza(nullptr)
{}

Celda::~Celda()
{
	delete _pieza;
}
