#ifndef TABLERO_H
#define TABLERO_H
#include <vector>
#include "Celda.h"
using std::vector;

class Tablero {
	public:
		Tablero(int numJugadores);
		virtual ~Tablero();
		Celda* map[17][13];
		vector<Celda*> getMovimientosPosibles(Celda* from);
	private:
		int _numPlayers;
		void createMap();
		vector<Celda*> celdaDisponible(int row, int col, int direction);
};

#endif
