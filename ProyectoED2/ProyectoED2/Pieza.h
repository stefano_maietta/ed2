#ifndef PIEZA_H
#define PIEZA_H
#include <iostream>
#include "Celda.h"
using namespace std;
class Celda;
class Pieza
{
public:
	Pieza(string val);
	virtual ~Pieza();
	inline void setVal(string val){ _val = val; }
	inline string getVal(){ return _val; }
	inline void setCelda(Celda* celda){ _celda = celda; }
	inline Celda* getCelda(){ return _celda; }
private:
	Celda* _celda;
	string _val;
};

#endif