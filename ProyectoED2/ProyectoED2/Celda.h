#ifndef CELDA_H
#define CELDA_H
#include "Pieza.h"
class Pieza;
class Celda
{
public:
	Celda(int fila, int columna, bool valid);
	virtual ~Celda();
	inline void setFila(int row){ _fila = row; }
	inline int getFila(){ return _fila; }
	inline void setColmna(int column){ _columna = column; }
	inline int getColumna(){ return _columna; }
	inline Pieza* getPieza(){ return _pieza; }
	inline void setPieza(Pieza*	pieza){ _pieza = pieza; }
	inline bool isEmpty(){ return _pieza == nullptr; }
	inline bool isValid(){ return _valid; }
	inline void clean(){ _pieza = nullptr; }
private:
	int _fila;
	int _columna;
	bool _valid;
	Pieza* _pieza;
};

#endif
