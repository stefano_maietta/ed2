#include "Tablero.h"
#include "Partida.h"
#include "Jugador.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <vector>
using namespace std;
using std::vector;

Partida* partida;
void gameLoop(); //Patr�n
Celda* examinarMovimientoJugador(vector<Celda*> moves, int row, int col);
Pieza* seleccionarFicha(Jugador* jugador);
void moverFicha(Jugador* player);
vector<Jugador*> instanciarJugadores(int numeroJugadores);
void imprimirTablero();
void dise�arMarcador(int numeroJugadores);


int main()
{
	int numeroJugadores = 2; //Para m�s adelante agregar m�s jugadores
	dise�arMarcador(numeroJugadores);
	partida->asignarPiezas();
	system("pause");
	system("cls");

	gameLoop();
	return 0;
}

void gameLoop() //Patr�n
{
	bool noSalir = true;
	Jugador* player = nullptr;
	while (noSalir)
	{
		cout << "------------------Damas Chinas---------------" << endl;
		imprimirTablero();
		player = partida->getJugadorActual();
		cout << "En juego: " << player->getNombre() << endl;
		moverFicha(player);
		system("pause");
		system("cls");
	}
}

void dise�arMarcador(int numeroJugadores)
{
	Tablero* tablero;
	vector<Jugador*> players = instanciarJugadores(numeroJugadores);

	tablero = new Tablero(numeroJugadores);
	partida = new Partida(players, tablero);
}

vector<Jugador*> instanciarJugadores(int numeroJugadores)
{
	vector<Jugador*> jugadores(numeroJugadores);
	Jugador* jugador;
	for (int i = 0; i < jugadores.size(); i++)
	{
		int cant = i + 1;
		std::string nombre = "Jugador" + std::to_string(cant);
		jugador = new Jugador(to_string(cant), nombre);	
		jugadores[i] = jugador;
		cout << jugadores[i]->getNombre() + "\n";
	}

	return jugadores;
}


void moverFicha(Jugador* jugador)
{
	Pieza* fichaSeleccionada = seleccionarFicha(jugador);
	if (fichaSeleccionada != nullptr)
	{
		//Testing Method
		vector<Celda*> movimiento = partida->getMovimientosPosibles(fichaSeleccionada->getCelda()->getFila(), fichaSeleccionada->getCelda()->getColumna());
		if (movimiento.size() > 0)
		{
			cout << "Posibles movimientos " << 3 << "||" << 6 << endl;
			for (Celda* celda : movimiento)
			{
				cout << "Fila: " << celda->getFila() << "|| Columna: " << celda->getColumna() << endl;
			}
			cout << "...................................................." << endl;
			int fila, columna;
			string filaSeleccionada, columnaSeleccionada;
			cout << "Seleccione la fila: ";
			cin >> filaSeleccionada;
			cout << "Selecciona la columna: ";
			cin >> columnaSeleccionada;
			fila = atoi(filaSeleccionada.c_str());
			columna = atoi(columnaSeleccionada.c_str());
			Celda* final = examinarMovimientoJugador(movimiento, fila, columna);
			if (final != nullptr)
			{
				partida->moverPieza(fichaSeleccionada, final);
			}
			else
			{
				cout << "Movimiento incorrecto" << endl;
			}
		}
		else
		{
			cout << "No es posible realizar ese movimiento" << endl;
		}
	}
	else
	{
		cout << "Movimiento incorrecto" << endl;
	}

}

Celda* examinarMovimientoJugador(vector<Celda*> movimiento, int fila, int columna)
{
	Celda* celdaNueva = nullptr;
	for (Celda* celda : movimiento)
	{
		if (celda->getFila() == fila && celda->getColumna() == columna)
		{
			celdaNueva = celda;
			break;
		}
	}
	return celdaNueva;
}


void imprimirTablero()
{
	cout << endl << endl;
	const Tablero* board = partida->getTablero();
	//Chinese Board
	string space;
	cout << "  0   1   2   3   4   5   6   7   8   9   10   11   12 " << endl;
	for (int i = 0; i < 17; i++)
	{
		cout << i;
		for (int j = 0; j < 13; j++)
		{
			if (board->map[i][j]->isValid() == false){
				cout << "    ";
			}void imprimirTablero();

				if (board->map[i][j]->isValid())
				{
					if (board->map[i][j]->getPieza() != nullptr){
						cout << "|" + board->map[i][j]->getPieza()->getVal() + "_|";
					}
					else{
						cout << "|__|";
					}

				}
		}
		cout << endl << endl;
	}
}

Pieza* seleccionarFicha(Jugador* player)
{
	Pieza* fichaSeleccionada = nullptr;
	int fila, columna;
	string filaSeleccionada, columnaSeleccionada;
	cout << "Seleccione la fila: ";
	cin >> filaSeleccionada;
	cout << "Seleccione la columna: ";
	cin >> columnaSeleccionada;
	fila = atoi(filaSeleccionada.c_str());
	columna = atoi(columnaSeleccionada.c_str());
	for (Pieza* ficha : player->getPiezas())
	{
		if (ficha->getCelda()->getFila() == fila && ficha->getCelda()->getColumna() == columna)
		{
			fichaSeleccionada = ficha;
			break;
		}
	}
	return fichaSeleccionada;
}
