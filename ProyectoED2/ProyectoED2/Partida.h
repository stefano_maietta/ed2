#ifndef PARTIDA_H
#define PARTIDA_H

#include <vector>
#include <ctime>
#include "Jugador.h"
#include "Tablero.h"

using std::vector;

class Partida
{

public:
	Partida(vector<Jugador*> jugadores, Tablero* tablero);
	void asignarPiezas();
	inline vector<Jugador*> getJugadores(){ return _jugadores; }
	inline Jugador* getJugadorActual(){ return _jugadorActual; }
	void moverPieza(Pieza* pieza, Celda* destino);
	inline const Tablero* getTablero() const { return _tablero; }
	inline void setTablero(Tablero* ptablero) { _tablero = ptablero; }
	vector<Celda*> getMovimientosPosibles(int row, int col);
	virtual ~Partida();

private:
	void init();
	Jugador* _jugadorActual;
	Tablero* _tablero;
	int _count = 0;
	vector<Jugador*> _jugadores;
	
};

#endif
